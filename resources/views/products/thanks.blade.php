@extends('layouts.frontLayout.front_design')
@section('content')
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Yhan You For Choosing Us</li>
            </ol>
        </div>
       
    </div>
</section>
<section id="do_action">
    <div class="container">
        <div class="heading text-center">
            @if(Session::has('flash_message_error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">X</button>
                <strong>
                    {!! session('flash_message_error') !!}
                </strong>
            </div>
            @endif 
            @if(Session::has('flash_message_success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">X</button>
                <strong>
                    {!! session('flash_message_success') !!}
                </strong>
            </div> 
            @endif 
            <h3>Your Order Has Been Placed</h3>
            <p>
               Your Order Number is: {{Session::get('order_id')}} && Total Amount is: ${{Session::get('grand_total')}}
            </p>
        </div>
    </div>
</section>
<!--/#do_action-->

    
@endsection

@php
    Session::forget('grand_total');
    Session::forget('order_id');
@endphp
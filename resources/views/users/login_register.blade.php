@extends('layouts.frontLayout.front_design')
@section('content')
	
<section id="form" ><!--form-->
    <div class="container" id="loginform">
        @if(Session::has('flash_message_error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">X</button>
                        <strong>
                            {!! session('flash_message_error') !!}
                        </strong>
                    </div>
                    @endif 
                    @if(Session::has('flash_message_success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">X</button>
                        <strong>
                            {!! session('flash_message_success') !!}
                        </strong>
                    </div>
                    @endif  
        <div class="row">
            <div class="col-sm-4 col-sm-offset-1">
                <div class="login-form"><!--login form-->
                    <h2>User Login </h2>
                    <form action="{{url('/user-login')}}" method="POST" id="loginForm" name="loginForm">
                        {{csrf_field()}}
                        
                        <input name="email" type="email" placeholder="Email Address" />
                        <input name="password" type="password" placeholder="Password" />
                        {{-- <span>
                            <input type="checkbox" class="checkbox"> 
                            Keep me signed in
                        </span> --}}
                        <button type="submit" class="btn btn-default gets">Login</button><br>
                        
                            <a class="" href="{{url('/forgot-password')}}" >Forgot Password</a>
                        
                    </form>
                </div><!--/login form-->
            </div>
            <div class="col-sm-1">
                <h2 class="or">OR</h2>
            </div>
            <div class="col-sm-4">
                <div class="signup-form"><!--sign up form-->   
                    <h2>Register Now!</h2>
                    <form action="{{url('/user-register')}}" method="POST" id="registerForm">
                        {{csrf_field()}}
                        <input name="name" id="name" type="text" placeholder="Name"/>
                        <input name="email" id="email"  type="email" placeholder="Email Address"/>
                        <input name="password" id="myPassword"  type="password" placeholder="Password"/>
                        <input name="conpassword" id="conpassword"  type="password" placeholder="Confirm Password"/>
                        <button type="submit" class="btn btn-default  gets">Signup</button>
                    </form>
                </div><!--/sign up form-->
            </div>
        </div>
    </div>
</section><!--/form-->


@endsection
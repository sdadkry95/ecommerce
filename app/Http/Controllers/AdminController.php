<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\User;
use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    //
    public function  login(Request $request){
        if ($request->isMethod('post') ) {
            $data = $request->input();
            $adminCount = Admin::where(['username'=>$data['username'],'password'=>md5($data['password']),'status'=>1])->count();
            if($adminCount>0){
                Session::put('adminSession', $data['username']);
                return redirect('/admin/dashboard')->with('flash_message_success','Login Successful');
               
            }else{
                    return redirect('/admin')->with('flash_message_error','Invalid username or password');

            } 
        }

        return view('admin.admin_login');

    }
    public function dashboard(){
        // if (Session::has('adminSession')) {
            #for simple login
        // }else{
        //     return redirect('/admin')->with('flash_message_error','Please Login To Access Dashboard');
        // }
        return view('admin.dashboard');
    }
    
    public function settings(){
        $adminDetails = Admin::where(['username'=>Session::get('adminSession')])->first();

        return view('admin.settings')->with(compact('adminDetails'));

    }

    public function checkPassword(Request $request){
        $data = $request->all();
        $current_password = $data['current_pwd'];
        $adminCount = Admin::where(['username'=>Session::get('adminSession'),'password'=>md5($data['current_pwd'])])->count();
        if($adminCount == 1){
            echo "true";die;
        }else{
            echo "false";die;
        }

    }
    public function updatePassword(Request $request){
        if ($request->isMethod('post')){
            $data = $request->all();
            // $current_password = $data['current_pwd']; //just to understand not used in code

            $adminCount = Admin::where(['username'=>Session::get('adminSession'),'password'=>md5($data['current_pwd'])])->count();
            
            // echo $adminCount;die;

            if($adminCount == 1){
                $password = md5($data['new_pwd']);
                Admin::where('username',Session::get('adminSession'))->update(['password'=>$password]);
                // dd($password);
                return redirect('/admin/settings')->with('flash_message_success','Password Changed Successfully');
            }else{
                return redirect('/admin/settings')->with('flash_message_error','Incorrect Current Password');

            }
        }

    
    }
    
    public function logout(){
        Session::flush();
        return redirect('/admin')->with('flash_message_success','Logged Out Successfully');

    }
    

}
